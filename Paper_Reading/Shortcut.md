# Zero-Shot Object Classification
### ECCV 2020
1. TF-vaegan: Sanath Narayan* , Akshita Gupta* , Fahad Shahbaz Khan, Cees G. M. Snoek, Ling Shao. "Latent Embedding Feedback and Discriminative Features for Zero-Shot Classification." ECCV (2020).  
①Code_Link：<https://github.com/akshitac8/tfvaegan>  
②Summary_Link  
2. "Region Graph Embedding Network for Zero-Shot Learning. " ECCV SPOTLIGHT(2020)  
①Code_Link：None  
②Summary_Link  
